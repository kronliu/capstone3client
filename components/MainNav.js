import { useContext } from "react";
import { Nav, Navbar } from "react-bootstrap";
import Link from "next/link";
import UserContext from "../UserContext";

export default function MainNav() {
  // document.querySelector(".nav-link").on("mouseover", (e) => {
  //   $(e.target).addClass("borderIn");
  // });

  // document.querySelector(".nav-link").on("mouseout", (e) => {
  //   $(e.target).removeClass("borderIn");
  // });

  const { user } = useContext(UserContext);

  return (
    <Navbar bg="light" expand="lg" className="navbar shadow">
      <Link href="/login">
        <a className="navbar-brand">
          <span className="text-success">Budget</span> Tracker
        </a>
      </Link>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="ml-auto">
          {user.id == null ? (
            <>
              <Link href="/register">
                <a
                  className="nav-link"
                  onMouseOver={(e) => e.target.classList.add("borderIn")}
                  onMouseOut={(e) => e.target.classList.remove("borderIn")}
                >
                  Register
                </a>
              </Link>
              <Link href="/login">
                <a
                  className="nav-link"
                  onMouseOver={(e) => e.target.classList.add("borderIn")}
                  onMouseOut={(e) => e.target.classList.remove("borderIn")}
                >
                  Login
                </a>
              </Link>
            </>
          ) : (
            <>
              <Link href="/insights">
                <a
                  className="nav-link"
                  onMouseOver={(e) => e.target.classList.add("borderIn")}
                  onMouseOut={(e) => e.target.classList.remove("borderIn")}
                >
                  Insights
                </a>
              </Link>
              <Link href="/categories">
                <a
                  className="nav-link"
                  onMouseOver={(e) => e.target.classList.add("borderIn")}
                  onMouseOut={(e) => e.target.classList.remove("borderIn")}
                >
                  Categories
                </a>
              </Link>
              <Link href="/records">
                <a
                  className="nav-link"
                  onMouseOver={(e) => e.target.classList.add("borderIn")}
                  onMouseOut={(e) => e.target.classList.remove("borderIn")}
                >
                  Records
                </a>
              </Link>
              <Link href="/logout">
                <a
                  className="nav-link"
                  onMouseOver={(e) => e.target.classList.add("borderIn")}
                  onMouseOut={(e) => e.target.classList.remove("borderIn")}
                >
                  Logout
                </a>
              </Link>
            </>
          )}
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
}
