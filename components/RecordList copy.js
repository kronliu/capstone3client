import { useContext, useState, useEffect } from "react";
import { Container, Card, Col, Row, Form } from "react-bootstrap";
import axios from "axios";
import UserContext from "../UserContext";
import moment from "moment";
import Swal from "sweetalert2";

export default function RecordList() {
  const {
    filter,
    search,
    deleteRecord,
    records,
    setRecords,
    categories,
    setCategories,
  } = useContext(UserContext);
  console.log(records);
  const [name, setName] = useState("");
  const [type, setType] = useState("");
  const [description, setDescription] = useState("");
  const [amount, setAmount] = useState("");
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const [dataId, setDataId] = useState("");

  async function removeRecord(e) {
    e.preventDefault();
    let recordId = e.currentTarget.getAttribute("data-id");

    const { value: archive } = await Swal.fire({
      title: "Type DELETE",
      input: "text",
      inputValue: "",
      showCancelButton: true,
      inputValidator: (value) => {
        if (value !== "DELETE") {
          return "You need to write DELETE!";
        }
      },
    });

    if (archive == "DELETE") {
      axios
        .delete(
          `${process.env.NEXT_PUBLIC_BASE_URL}/api/user/records/${recordId}`,
          {
            headers: {
              Authorization: `Bearer ${localStorage.getItem("token")}`,
            },
          }
        )
        .then((res) => {
          if (res.data == true) {
            axios
              .get(`${process.env.NEXT_PUBLIC_BASE_URL}/api/user/details`, {
                headers: {
                  Authorization: `Bearer ${localStorage.getItem("token")}`,
                },
              })
              .then((res) => {
                setCategories(
                  res.data.categories.map((element) => {
                    return element;
                  })
                );
                setRecords(
                  res.data.records.map((element) => {
                    return element;
                  })
                );
              });
          }
        });
    }
  }

  function reviseRecord(e) {
    e.preventDefault();
    console.log(e);
    axios
      .put(
        `${process.env.NEXT_PUBLIC_BASE_URL}/api/user/records/${dataId}`,
        {
          name: name,
          type: type,
          description: description,
          amount: amount,
        },
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      )
      .then((res) => {
        console.log(res.data);
        if (res.data == true) {
          Swal.fire({
            icon: "success",
            title: "Record Update",
          });
          axios
            .get(`${process.env.NEXT_PUBLIC_BASE_URL}api/user/details`, {
              headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`,
              },
            })
            .then((res) => {
              setCategories(
                res.data.categories.map((element) => {
                  return element;
                })
              );
              setRecords(
                res.data.records.map((element) => {
                  return element;
                })
              );
            });
        }
      });
  }

  function getBudgetAtIndex(index) {
    let incomeAtIndex = 0;
    let expenseAtIndex = 0;
    let budgetAtIndex = 0;
    for (let i = 0; i < index; i++) {
      if (records[i].type == "Expense") {
        expenseAtIndex += records[i].amount;
      }
      if (records[i].type == "Income") {
        incomeAtIndex += records[i].amount;
      }
    }
    console.log(incomeAtIndex);
    console.log(expenseAtIndex);
    budgetAtIndex = incomeAtIndex - expenseAtIndex;
    return budgetAtIndex;
  }

  return (
    <section className="">
      <Container>
        {records
          .map((item) => item)
          .reverse()
          .filter((element) => {
            if (filter == "" || filter == "None") return element;
            if (filter == "Expense") return element.type == "Expense";
            if (filter == "Income") return element.type == "Income";
          })
          .filter((element) => {
            if (search == "") return element;
            if (search !== "")
              return element.name.toLowerCase().includes(search.toLowerCase());
          })
          .map((element) => {
            return (
              <Row
                key={element._id}
                className={
                  deleteRecord
                    ? "record-box-delete my-4 shadow-sm"
                    : "record-box my-4 shadow-sm"
                }
                onClick={deleteRecord ? (e) => removeRecord(e) : null}
                data-id={element._id}
              >
                <Col>
                  <Row>
                    <h5>{element.name}</h5>
                    {element.type == "Expense" ? (
                      <span className="ml-auto box-header-expense">
                        {element.type}
                      </span>
                    ) : null}
                    {element.type == "Income" ? (
                      <span className="ml-auto box-header-income">
                        {element.type}
                      </span>
                    ) : null}
                  </Row>
                  <Row>
                    <p>
                      {moment(element.addedOn).format("MMMM Do YY, h:mm a")}
                    </p>
                    <span
                      className={
                        element.type == "Expense"
                          ? "text-danger ml-auto my-3"
                          : "text-success ml-auto my-3"
                      }
                    >
                      {element.amount}
                    </span>
                  </Row>
                  <Row>
                    <button
                      data-id={element._id}
                      className="button blue"
                      onClick={(e) => {
                        setDataId(e.target.getAttribute("data-id"));
                        handleShow();
                      }}
                    >
                      Update
                    </button>
                    <span className="ml-auto">
                      {element.type == "Income"
                        ? getBudgetAtIndex(records.indexOf(element)) +
                          element.amount
                        : getBudgetAtIndex(records.indexOf(element)) -
                          element.amount}
                    </span>
                  </Row>
                </Col>
              </Row>
            );
          })}
      </Container>
      <div
        id="reviseRecordModal"
        className={show ? "modal modal-show" : "modal"}
      >
        <div className="modal-content bg-grey">
          <Form onSubmit={(e) => reviseRecord(e)}>
            <Row className="modal-body py-4 px-5">
              <Container>
                <Row className="">
                  <Col>
                    <h5 className="modal-title mb-4 text-muted">
                      Update Record
                    </h5>
                  </Col>
                </Row>

                <Form.Group controlId="type">
                  <Form.Control
                    className="select"
                    required
                    as="select"
                    value={type}
                    onChange={(e) => setType(e.target.value)}
                  >
                    <option value="Expense">Expense</option>
                    <option value="Income">Income</option>
                  </Form.Control>
                </Form.Group>
                <Form.Group controlId="name">
                  <Form.Control
                    required
                    className="select"
                    as="select"
                    value={name}
                    onChange={(e) => setName(e.target.value)}
                  >
                    <option value="" selected disabled>
                      Category name
                    </option>
                    {type == "Expense"
                      ? categories
                          .filter((element) => element.type == "Expense")
                          .map((element) => (
                            <option key={element._id}>{element.name}</option>
                          ))
                      : null}
                    {type == "Income"
                      ? categories
                          .filter((element) => element.type == "Income")
                          .map((element) => (
                            <option key={element._id}>{element.name}</option>
                          ))
                      : null}
                  </Form.Control>
                </Form.Group>
                <Form.Group controlId="description">
                  <Form.Control
                    className="input"
                    type="text"
                    placeholder="Description"
                    value={description}
                    onChange={(e) => setDescription(e.target.value)}
                  ></Form.Control>
                </Form.Group>
                <Form.Group controlId="amount">
                  <Form.Control
                    className="input"
                    required
                    type="number"
                    placeholder="Amount"
                    value={amount}
                    onChange={(e) => setAmount(e.target.value)}
                  ></Form.Control>
                </Form.Group>
                <Row>
                  <Col className="d-flex justify-content-end">
                    <div className="button ml-auto" onClick={handleClose}>
                      Close
                    </div>
                    <button className="button ml-4 mr-0" type="submit">
                      Update
                    </button>
                  </Col>
                </Row>
              </Container>
            </Row>
          </Form>
        </div>
      </div>
    </section>
  );
}
