import { useState, useEffect } from "react";
import Router from "next/router";
import { Form, Button, Col, Row, Container, Card } from "react-bootstrap";
import axios from "axios";
import Swal from "sweetalert2";

export default function RegisterForm() {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [password2, setPassword2] = useState("");
  const [mobileNo, setMobileNo] = useState("");
  const [isActive, setIsActive] = useState(false);

  useEffect(() => {
    if (
      firstName !== "" &&
      lastName !== "" &&
      email !== "" &&
      password !== "" &&
      password2 !== "" &&
      mobileNo !== "" &&
      password2 === password
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password, password2, mobileNo, firstName, lastName]);

  function registerUser(e) {
    e.preventDefault();
    console.log(e);
    fetch(`https://dry-caverns-57051.herokuapp.com/api/user/email-exists`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ email: email }),
    })
    .then((res) => res.json())
    .then((res) => {
      console.log(res);
      if (res === false) {
        fetch(`https://dry-caverns-57051.herokuapp.com/api/user/`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            firstName: firstName,
            lastName: lastName,
            email: email,
            password: password,
            mobileNo: mobileNo,
          }),
        })
          .then((res) => res.json())
          .then((res) => {
            if (res === true) {
              Swal.fire({
                icon: "success",
                title: "Registration Successful",
              });
              Router.push("/login");
            } else {
              Swal.fire({
                icon: "error",
                title: "Registration Failed",
              });
            }
          });
      } else {
        Swal.fire({
          icon: "error",
          title: "Email Already Exists",
        });
      }
    });

    setFirstName("");
    setLastName("");
    setEmail("");
    setPassword("");
    setPassword2("");
    setMobileNo("");
  }

  return (
    <>
      <section>
        <Container className="pt-lg-7">
          <Row className="justify-content-center">
            <Col lg="5">
              <Card className="bg-grey shadow border-0 card-container">
                <Card.Body className="px-lg-5 py-lg-5">
                  <div className="text-muted text-center mb-3">
                    <small>Create new account</small>
                  </div>
                  <Form onSubmit={(e) => registerUser(e)}>
                    <Row>
                      <Col xs={12} lg={6}>
                        <Form.Group controlId="firstName">
                          <Form.Control
                            className="input"
                            type="text"
                            placeholder="First name"
                            value={firstName}
                            onChange={(e) => setFirstName(e.target.value)}
                            required
                          />
                        </Form.Group>
                      </Col>
                      <Col xs={12} lg={6}>
                        <Form.Group controlId="lastName">
                          <Form.Control
                            className="input"
                            type="text"
                            placeholder="Last name"
                            value={lastName}
                            onChange={(e) => setLastName(e.target.value)}
                            required
                          />
                        </Form.Group>
                      </Col>
                    </Row>
                    <Row>
                      <Col xs={12}>
                        <Form.Group controlId="email">
                          <Form.Control
                            className="input"
                            type="email"
                            placeholder="Email"
                            value={email}
                            onChange={(e) => setEmail(e.target.value)}
                            required
                          />
                        </Form.Group>
                      </Col>
                    </Row>
                    <Row>
                      <Col xs={12}>
                        <Form.Group controlId="password">
                          <Form.Control
                            className="input"
                            type="password"
                            placeholder="Password"
                            value={password}
                            onChange={(e) => setPassword(e.target.value)}
                            required
                          />
                        </Form.Group>
                      </Col>
                      <Col xs={12}>
                        <Form.Group controlId="password2">
                          <Form.Control
                            className="input"
                            type="password"
                            placeholder="Verify Password"
                            value={password2}
                            onChange={(e) => setPassword2(e.target.value)}
                            required
                          />
                        </Form.Group>
                      </Col>
                    </Row>
                    <Row>
                      <Col xs={12}>
                        <Form.Group controlId="mobileNo">
                          <Form.Control
                            className="input"
                            type="number"
                            placeholder="Mobile number"
                            value={mobileNo}
                            onChange={(e) => setMobileNo(e.target.value)}
                            required
                          />
                        </Form.Group>
                      </Col>
                    </Row>

                    {isActive ? (
                      <button className="button" type="submit">
                        Submit
                      </button>
                    ) : (
                      <button className="button" type="submit" disabled>
                        Submit
                      </button>
                    )}
                  </Form>
                </Card.Body>
              </Card>
            </Col>
          </Row>
        </Container>
      </section>
    </>
  );
}
