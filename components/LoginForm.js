import { useState, useContext } from "react";
import Router from "next/router";
import { Form, Button, Container, Row, Col, Card } from "react-bootstrap";
import axios from "axios";
import UserContext from "../UserContext";
import Swal from "sweetalert2";
import { GoogleLogin } from "react-google-login";

export default function LoginForm() {
  const { setUser, setCategories, setRecords } = useContext(UserContext);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [email1, setEmail1] = useState("");
  const [password1, setPassword1] = useState("");
  const [password2, setPassword2] = useState("");
  const [show, setShow] = useState(false);

  const authenticateGoogleToken = (response) => {
    console.log(response);
    fetch(
      `https://dry-caverns-57051.herokuapp.com/api/user/verify-google-id-token`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          tokenId: response.tokenId,
          googleToken: response.accessToken,
        }),
      }
    )
      .then((res) => res.json())
      .then((res) => {
        if (typeof res.access !== "undefined") {
          console.log("User has a valid token");
          localStorage.setItem("token", res.access);
          axios
            .get(`https://dry-caverns-57051.herokuapp.com/api/user/details`, {
              headers: {
                Authorization: `Bearer ${res.access}`,
              },
            })
            .then((res) => {
              setUser({
                id: res._id,
              });
              Swal.fire({
                icon: "success",
                title: "You are now logged in",
              });
              Router.push("/records");
            });
        } else {
          if (res.error === "google-auth-alert") {
            Swal.fire({
              icon: "error",
              title: "Google Authentication Error",
              text:
                "Google Authentication Procedure has failed, try again or contact your web admin",
            });
          } else if (res.error === "login-type-error") {
            Swal.fire({
              icon: "error",
              title: "Login Type Error",
              text:
                "You may have registered using a difyrent login procedure. Try alternative login process.",
            });
          }
        }
      });
  };

  function authenticate(e) {
    e.preventDefault();

    fetch(`https://dry-caverns-57051.herokuapp.com/api/user/login`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    })
      .then((res) => res.json())
      .then((res) => {
        if (res.access) {
          localStorage.setItem("token", res.access);
          fetch(`https://dry-caverns-57051.herokuapp.com/api/user/details`, {
            headers: {
              "Content-Type": "application/json",
              Authorization: `Bearer ${res.access}`,
            },
          })
            .then((res) => res.json())
            .then((res) => {
              setUser({
                id: res._id,
              });
              setCategories(
                res.categories.map((element) => {
                  return element;
                })
              );
              setRecords(
                res.records.map((element) => {
                  return element;
                })
              );
              Router.push("/records");
            });
        } else {
          Swal.fire({
            icon: "error",
            title: "Invalid input",
          });
        }
      });
  }

  function rePassword(e) {
    e.preventDefault();
    console.log(e);
    if (password1 == password2) {
      fetch(`https://dry-caverns-57051.herokuapp.com/api/user/forgot`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ email: email1 }),
      })
        .then((res) => res.json())
        .then((res) => {
          if (res !== true) {
            Swal.fire({
              icon: "success",
              title: "Password updated",
            });
            fetch(
              `https://dry-caverns-57051.herokuapp.com/api/user/forgot-password`,
              {
                method: "POST",
                headers: {
                  "Content-Type": "application/json",
                  Authorization: `Bearer ${res.data.access}`,
                },
                body: JSON.stringify({
                  password: password1,
                }),
              }
            ).then((res) => {
              console.log(res.data);
            });
          } else {
            Swal.fire({
              icon: "error",
              title: "Invalid input",
            });
          }
        });
    } else {
      Swal.fire({
        icon: "error",
        title: "Passwords do not match",
      });
    }
  }

  return (
    <>
      <section>
        <Container className="pt-lg-7">
          <Row className="justify-content-center">
            <Col lg="5">
              <Card className="shadow border-0 card-container">
                <Card.Header className="bg-white pb-5 d-flex flex-column justify-content-center align-items-center">
                  <div className="text-muted text-center mb-3">
                    <small>Sign in with</small>
                  </div>
                  <GoogleLogin
                    clientId="832716342650-4g78a8e35br96buh2h9ff53oqv9ssruv.apps.googleusercontent.com"
                    buttonText="Login with Google"
                    cookiePolicy={"single_host_origin"}
                    onSuccess={authenticateGoogleToken}
                    onFailure={authenticateGoogleToken}
                  />
                </Card.Header>
                <Card.Footer className="px-lg-5 py-lg-5 bg-grey">
                  <div className="text-muted text-center mb-3">
                    <small>Or with other credentials</small>
                  </div>
                  <Form onSubmit={(e) => authenticate(e)}>
                    <Form.Group controlId="email">
                      <Form.Control
                        className="input"
                        type="email"
                        placeholder="Email"
                        value={email}
                        onChange={(e) => {
                          setEmail(e.target.value);
                        }}
                      ></Form.Control>
                    </Form.Group>
                    <Form.Group controlId="password">
                      <Form.Control
                        className="input"
                        type="password"
                        placeholder="Password"
                        value={password}
                        onChange={(e) => {
                          setPassword(e.target.value);
                        }}
                      ></Form.Control>
                    </Form.Group>
                    <button type="submit" className="button">
                      Sign In
                    </button>
                    <span className="">
                      <small className="ml-3 re-password" onClick={handleShow}>
                        Forgot password?
                      </small>
                    </span>
                  </Form>
                </Card.Footer>
              </Card>
            </Col>
          </Row>
        </Container>
        <div
          id="updatePasswordModal"
          className={show ? "modal modal-show" : "modal"}
        >
          <div className="modal-content bg-grey">
            <Form onSubmit={(e) => rePassword(e)}>
              <Row className="modal-body py-4 px-5">
                <Container>
                  <Row className="">
                    <Col>
                      <h5 className="modal-title mb-4 text-muted">
                        Update Password
                      </h5>
                    </Col>
                  </Row>

                  <Form.Group controlId="email">
                    <Form.Control
                      required
                      className="input"
                      type="email"
                      placeholder="Email"
                      value={email1}
                      onChange={(e) => setEmail1(e.target.value)}
                    ></Form.Control>
                  </Form.Group>
                  <Form.Group controlId="password1">
                    <Form.Control
                      required
                      className="input"
                      type="password"
                      placeholder="New password"
                      value={password1}
                      onChange={(e) => setPassword1(e.target.value)}
                    ></Form.Control>
                  </Form.Group>
                  <Form.Group controlId="password2">
                    <Form.Control
                      required
                      className="input"
                      type="password"
                      placeholder="Verify password"
                      value={password2}
                      onChange={(e) => setPassword2(e.target.value)}
                    ></Form.Control>
                  </Form.Group>
                  <Row>
                    <Col className="d-flex justify-content-end">
                      <div className="button ml-auto" onClick={handleClose}>
                        Close
                      </div>
                      <button className="button ml-4 mr-0" type="submit">
                        Update Password
                      </button>
                    </Col>
                  </Row>
                </Container>
              </Row>
            </Form>
          </div>
        </div>
      </section>
    </>
  );
}
