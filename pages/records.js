import { useState, useEffect, useContext } from "react";
import RecordList from "../components/RecordList";
import CreateRecord from "../components/CreateRecord";

import DeleteRecord from "../components/DeleteRecord";
import FilterRecords from "../components/FilterRecords";
import SearchRecords from "../components/SearchRecords";
import { Row, Col, Container, Card } from "react-bootstrap";
import axios from "axios";
import UserContext from "../UserContext";

export default function categories() {
  const { records, categories, setRecords, setCategories } = useContext(
    UserContext
  );

  return (
    <>
      <section className="text-center d-flex flex-column align-items-center justify-content-center background-img-fixed page-section py-5">
        <h1 className="text-white">Budget Tracker</h1>
        <p className="lead text-white">
          Est ex in ad nulla laborum voluptate enim voluptate labore.
        </p>
        <Row className="">
          <Col className="ml-2">
            <CreateRecord />
          </Col>
          <Col>
            <DeleteRecord />
          </Col>

          <Col>
            <FilterRecords />
          </Col>
          <Col>
            <SearchRecords />
          </Col>
        </Row>
        <Container className="pt-lg-7">
          <Row className="d-flex flex-wrap justify-content-center align-items-center">
            <Col lg={12} className="m-5">
              <Card className="bg-white shadow border-0 card-container ">
                <Card.Body className="px-lg-5 py-lg-5">
                  <RecordList />
                </Card.Body>
              </Card>
            </Col>
          </Row>
        </Container>
      </section>
    </>
  );
}
