import { useState, useEffect } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "../styles/globals.css";
import "../styles/app.scss";
import "react-datepicker/dist/react-datepicker.css";
import { Container } from "react-bootstrap";
import MainNav from "../components/MainNav";
import { UserProvider } from "../UserContext";

import axios from "axios";

function MyApp({ Component, pageProps }) {
  const [records, setRecords] = useState([]);
  const [categories, setCategories] = useState([]);
  const [filter, setFilter] = useState("");
  const [search, setSearch] = useState("");
  const [deleteRecord, setDeleteRecord] = useState("");
  const [user, setUser] = useState({
    id: null,
  });
  const unsetUser = () => {
    localStorage.clear();

    setUser({
      id: null,
    });
  };

  useEffect(() => {
    fetch(`https://dry-caverns-57051.herokuapp.com/api/user/details`, {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((res) => {
        if (res._id) {
          setUser({
            id: res._id,
          });
        } else {
          setUser({
            id: null,
          });
        }
      });
  }, []);

  useEffect(() => {
    if (localStorage.getItem("token") !== null) {
      fetch(`https://dry-caverns-57051.herokuapp.com/api/user/details`, {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      })
        .then((res) => res.json())
        .then((res) => {
          console.log(res.data);
          setCategories(
            res.categories.map((element) => {
              return element;
            })
          );
          setRecords(
            res.records.map((element) => {
              return element;
            })
          );
        });
    }
  }, []);

  return (
    <>
      <UserProvider
        value={{
          user,
          setUser,
          unsetUser,
          records,
          setRecords,
          categories,
          setCategories,
          filter,
          setFilter,
          search,
          setSearch,
          deleteRecord,
          setDeleteRecord,
        }}
      >
        <MainNav />

        <Component {...pageProps} />
      </UserProvider>
    </>
  );
}

export default MyApp;
