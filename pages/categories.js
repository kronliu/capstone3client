import CreateCategory from "../components/CreateCategory";

export default function categories() {
  return (
    <section className="background-img page-section py-5">
      <CreateCategory />;
    </section>
  );
}
