import RegisterForm from "../components/RegisterForm";
import Head from "next/head";
export default function login() {
  return (
    <>
      <Head>Login || Budget Tracker</Head>
      <section className="background-img page-section py-5">
        <RegisterForm />
      </section>
    </>
  );
}
